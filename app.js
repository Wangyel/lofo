const express = require("express") // packages
const app = express() // import server

const path = require("path") //package for path
const cookieParser = require("cookie-parser")

app.use(express.json())
app.use(express.static(path.join(__dirname,"view"))) //connecting frontend

require("./dataBase/mongoose") //connecting to database(calling the database)

const userRouter = require("./routes/userRoutes") // importing the route 
const adminRouter = require("./routes/adminRoutes")

app.use(cookieParser()) // server to use the cookieparser
app.use(userRouter) // server to use the route
app.use(adminRouter)

module.exports = app;