const sharp = require("sharp")
const userModel = require("../model/userModel")
const itemModel = require("../model/itemModel")
const feedbackModel = require("../model/feedbackModel")

const userSignup = async (req,res) => {
    try{
         // retrive data from req body
        const data = req.body 
        const user = new userModel(data) // new lets you create a row or say document
        await user.save() // save to database 

        const authToken = await user.generateAuthToken() // generate token
        res.status(200).send({data:data,token:authToken})
    }catch(e){
        res.status(400).send({message:e})
    }
}

const userLogin = async (req,res) => {
    try{
        const data = req.body
        const user = await userModel.findByCredentials(data.email,data.password) // cross checking password and user existence
        const token = await user.generateAuthToken() // crate a token and save it in user
        res.cookie("session_id",token) // send the cookie
        // token has the user id
        res.status(200).send({message:user,loginToken:token})
    }catch(e){
        res.status(400).send({message:e})
    }
}

const userLogOut = async (req, res) => {
    try{
        req.user.tokens = req.user.tokens.filter((token) => { // deleting token after signout
            return token.token !== req.token
        })

        await req.user.save() // saving the new user data
        
        res.status(200).send({message:"User Logged out"})
    }catch(e){
        res.status(400).send({message:e})
    }
}

const createAvatar = async (req, res) => {
    try{
        const buffer = await sharp(req.file.buffer).resize({width:250, height:250}).png().toBuffer()
        req.user.avatar = buffer
        await req.user.save();
        res.send();
    }catch(e){
        res.status(400).send(e)
    }
}
const getAvatar = async (req, res) => {
    try{
        const user2 = await userModel.findById(req.params.id)

        if(!user2 || !user2.avatar) {
            throw new Error()
        }

        res.set('Content-Type', 'image/jpg')
        res.send(user2.avatar)
    } catch(e) {
        res.status(400).send()
    }
}

const deleteAvatar = async(req,res)=> {
    try{
        req.user.avatar = undefined
        await req.user.save()
        res.send()
    }catch(e){
        req.status(400).send(e)
    }
}

// Controller method to create a new item
const createItem = async (req, res) => {
    try {
      const buffer = await sharp(req.file.buffer).resize({width:250, height:250}).png().toBuffer()

      const data = {
        date:req.body.date,
        time:req.body.time,
        location:req.body.location,
        owner:req.body.owner,
        contact:req.body.contact,
        designation:req.body.designation,
        description:req.body.description,
        category:req.body.category,
        avatar:buffer
      }

      const newItem = new itemModel(data);
      await newItem.save();
      res.status(201).json(newItem);
    } catch (error) {
      res.status(400).json({ error: error.message });
    }
  };

  // Update an item by ID
const updateItem = async (req, res) => {
    const itemId = req.params.itemId; // Assuming itemId is passed as a route parameter
    const updates = req.body; // Data to update
  
    try {
      const item = await itemModel.findByIdAndUpdate(itemId, updates);
      if (!item) {
        return res.status(404).json({ error: 'Item not found' });
      }
      res.json(item);
    } catch (error) {
      res.status(400).json({ error: error.message });
    }
  };

  // Get a single item by ID
const getItemById = async (req, res) => {
    const itemId = req.params.itemId; // Assuming itemId is passed as a route parameter
  
    try {
      const item = await itemModel.findById(itemId);
      if (!item) {
        return res.status(404).json({ error: 'Item not found' });
      }
      res.json(item);
    } catch (error) {
      res.status(400).json({ error: error.message });
    }
  };

// Get all items
const getAllItems = async (req, res) => {
    try {
      const items = await itemModel.find({category:req.params.category});
      res.json(items);
    } catch (error) {
      res.status(400).json({ error: error.message });
    }
  };

// Delete an item by ID
const deleteItemById = async (req, res) => {
    const itemId = req.params.itemId; // Assuming itemId is passed as a route parameter
  
    try {
      const item = await itemModel.findByIdAndDelete(itemId);
      if (!item) {
        return res.status(404).json({ error: 'Item not found' });
      }
      res.json({ message: 'Item deleted successfully' });
    } catch (error) {
      res.status(400).json({ error: error.message });
    }
};

//feedback
const createFeedback = async (req,res) => {
  try{
      const feedback = new feedbackModel(req.body)
      await feedback.save()

      res.status(200).send({message:feedback})
  }catch(e){
      res.status(400).send({message:e})
  }
}

//get the item
const getItemImage = async (req, res) => {
  try{
      const item = await itemModel.findById(req.params.id)

      if(!item || !item.avatar) {
        throw new Error('Item not found or does not have an avatar');
      }

      res.set('Content-Type', 'image/png')
      res.send(item.avatar)
  } catch(e) {
    res.status(400).json({ error: 'Item image not found' });
  }
}


module.exports = {userSignup, userLogin, userLogOut, getAvatar, createAvatar, deleteAvatar, createItem, updateItem, getItemById, getAllItems, deleteItemById,createFeedback, getItemImage}

