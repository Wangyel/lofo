const adminModel = require("../model/adminModel")

const adminSignup = async (req,res) => {
    try{
        const data = req.body
        const admin = new adminModel(data)

        await admin.save() // save to database  

        const authToken = await admin.generateAuthToken()

        res.status(200).send({data:data, token:authToken})
    }catch(e){
        res.status(400).send({message:e})
    }
}

const adminLogin = async(req, res) => {
    try{
        const data = req.body
        const admin = await adminModel.findByCredentials(data.email,data.password) // cross checking password and user existence
        const token = await admin.generateAuthToken()

        res.cookie("session_id",token)
        res.status(200).send({message:admin, loginToken:token})
    }catch(e){
        res.status(400).send({message:e})
    }
}

const adminLogOut = async (req, res) => {
    try{
        req.admin.tokens = req.admin.tokens.filter((token) => { // deleting token after signout
            return token.token !== req.token
        })

        await req.admin.save() // saving the new user data
        
        res.status(200).send({message:"Admin Logged out"})
    }catch(e){
        res.status(400).send({message:e})
    }
}

module.exports = {adminSignup, adminLogOut, adminLogin}