const app = require("./app"); // import applications from app.js

require("dotenv").config({path:'./configuration/config.env'})
const port = process.env.PORT; // setting the server

app.listen(port, () => {
  // turning on the server
  console.log(`Server running on port ${port}...`);
});
