function validateForm(){
    var selectedDate = document.getElementById('date').value;
    var currentDate = new Date().toISOString().split('T')[0];
  
    if (selectedDate >= currentDate){
      alert("please select a date before today");
      return false;
    }
    return true;
  }
  
  
  + function($) {
    $('.palceholder').click(function() {
      $(this).siblings('input').focus();
    });
  
    $('.form-control').focus(function() {
      $(this).parent().addClass("focused");
    });
  
    $('.form-control').blur(function() {
      var $this = $(this);
      if ($this.val().length == 0)
        $(this).parent().removeClass("focused");
    });
    $('.form-control').blur();
  
    // validetion
    $.validator.setDefaults({
      errorElement: 'span',
      errorClass: 'validate-tooltip'
    });
  
    $("#formvalidate").validate({
      rules: {
        date: {
          required: true,
        },
        location: {
          required: true,
        },
        contact: {
          required: true,
        },
        description:{
          required: true,
        }
      },
      messages: {
        date: {
          required: "This field is required.",
        },
        location: {
          required: "This field is required.",
        },
        contact: {
          required: "This field is required.",
        },
        description: {
          required: "This field is required.",
        },
        
  
      }
    });
  
  }(jQuery);
  var btnUpload = $("#upload_file"),
    btnOuter = $(".button_outer");
  btnUpload.on("change", function (e) {
    var ext = btnUpload.val().split(".").pop().toLowerCase();
    if ($.inArray(ext, ["gif", "png", "jpg", "jpeg"]) == -1) {
      $(".error_msg").text("Not an Image...");
    } else {
      $(".error_msg").text("");
      btnOuter.addClass("file_uploading");
      setTimeout(function () {
        btnOuter.addClass("file_uploaded");
      }, 3000);
      var uploadedFile = URL.createObjectURL(e.target.files[0]);
      setTimeout(function () {
        $("#uploaded_view")
          .append('<img src="' + uploadedFile + '" />')
          .addClass("show");
      }, 3500);
    }
  });
  $(".file_remove").on("click", function (e) {
    $("#uploaded_view").removeClass("show");
    $("#uploaded_view").find("img").remove();
    btnOuter.removeClass("file_uploading");
    btnOuter.removeClass("file_uploaded");
  });
  document.getElementById("formvalidate").addEventListener("submit", function(event) {
    event.preventDefault(); // Prevent form submission
  
  var formData = new FormData()
  
  //get values from
  var date = document.getElementById("date").value;
  var time = document.getElementById("time").value;
  var location = document.getElementById("location").value;
  var owner = document.getElementById("owner").value;
  var contact = document.getElementById("contact").value;
  var designation = document.getElementById("designation").value;
  var description = document.getElementById("description").value;
  var category = "lost"
  var image = document.getElementById("upload_file").files[0];
  
  console.log(formData)
  
  //append values to the FormData object
  formData.append("date", date)
  formData.append("time", time)
  formData.append("location", location)
  formData.append("owner", owner)
  formData.append("contact", contact)
  formData.append("designation", designation)
  formData.append("description", description)
  formData.append("category", category)
  formData.append("avatar", image)
  
  fetch("/users/item", {
    method: "POST",
    body: formData
  })
  .then((res) =>  {
    if (res.status === 201) {
      alert("Lost report succesful");
      // Handle the success response from the server
  } else {
      throw new Error("Sorry something went wrong. Please try again.");
  }
  })
  .catch((error) => {
    alert(error);
  });
  });
