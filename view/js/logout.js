function logout() {
    // Ask the user for confirmation
    if (confirm("Are you sure you want to log out?")) {
        fetch("/user/logout", {
            method: "POST"
        }).then(res => {
            if (res.ok) {
                window.open("../index.html", "_self");
            }
        }).catch(e => console.log(e));
    }
}

function adminLogout() {
    if (confirm("Are you sure you want to log out?")) {
        fetch("/admin/logout", {
            method: "POST"
        }).then(res => {
            if (res.ok) {
                window.open("../index.html", "_self");
            }
        }).catch(e => console.log(e));
    }
}
