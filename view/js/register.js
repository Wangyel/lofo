+ function($) {
  $('.palceholder').click(function() {
    $(this).siblings('input').focus();
  });

  $('.form-control').focus(function() {
    $(this).parent().addClass("focused");
  });

  $('.form-control').blur(function() {
    var $this = $(this);
    if ($this.val().length == 0)
      $(this).parent().removeClass("focused");
  });
  $('.form-control').blur();

  // validetion
  $.validator.setDefaults({
    errorElement: 'span',
    errorClass: 'validate-tooltip'
  });

  $("#formvalidate").validate({
    rules: {
      name:{
        required:true
      },
      email: {
        required: true,
      },
      password: {
        required: true,
        minlength: 8
      },
      password1:{
        required: true,
        equalTo: "#password"
      }
    },
    messages: {
      email: {
        required: "Please enter your email.",
        minlength: "Please provide valid email.",
        must:"Must contain @."
      },
      password: {
        required: "Enter your password.",
        minlength: "password must atleast 8."
      },
      password1:{
        required:"Enter your confirm password",
        equalTo:'Password does not match'
      }
    }
  });

}(jQuery);
document.getElementById("formvalidate").addEventListener("submit", function(event) {
  event.preventDefault(); // Prevent form submission

  // Get form field values
  const firstName = document.getElementById("fname").value;
  const lastName = document.getElementById("lname").value;
  const email = document.getElementById("email").value;
  const password = document.getElementById("password").value;
  const confirmPassword = document.getElementById("confirmpassword").value;

  const userData = {
    firstName: firstName,
    lastName: lastName,
    email: email,
    password: password
  };

   console.log(userData)
  // // Perform form validation and submit the data to the backend
  // // You can use JavaScript fetch or any other method to send the data to the server
  // if (password !== confirmPassword) {
  //   alert("Password and Confirm Password must match.");
  // } else {

  //   // Send the userData to the backend
  //   // Example: fetch('/user/signup', { method: 'POST', body: JSON.stringify(userData) })
  //   // Handle the response as needed
  //   console.log(userData);
  // }
  fetch("/user/signup", {
    method: "POST",
    headers: {"Content-Type": "application/json; charset=UTF-8" },
    body: JSON.stringify(userData)
  })
  .then((res) =>  {
    if (res.ok) {
      alert("User registered successfully");
      // Handle the success response from the server
    } else {
      throw new Error("Error registering user");
    }
  })
  .catch((error) => {
    alert(error);
  });
});
