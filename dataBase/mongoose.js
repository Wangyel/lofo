const mongoose = require("mongoose")

require("dotenv").config({path:'./configuration/config.env'})
const DB = process.env.DATABASE_LOCAL // setting the port for database(database configuration)

mongoose.connect(DB) // connecting with the database
    .then((con) => {// if succesful
        console.log(con.connections)
        console.log("DB connect successful")
    })
    .catch((error) => {
        console.log(error)
    })