const express = require("express")
const router = express.Router()// set up the router
const auth = require("../middleWare/auth")

const controller = require("../controller/admin")

router.post("/admin/signup", controller.adminSignup)
router.post("/admin/login",controller.adminLogin)
router.post("/admin/logout",auth.adminAuth,controller.adminLogOut)


module.exports = router;