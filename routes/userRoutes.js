const multer = require("multer")
const express = require("express");
const sharp = require('sharp')
const router = express.Router();
const auth = require("../middleWare/auth")


const controller = require("../controller/user");
const user = require("../model/userModel");

router.post("/user/signup", controller.userSignup);
router.post("/user/login",controller.userLogin);
router.post("/user/logout",auth.userAuth, controller.userLogOut)


const upload = multer({
    limits: {
      fileSize: 20000000
    },
    fileFilter(req, file, cb) {
      if (!file.originalname.match(/\.(jpg|jpeg|png)$/)) {
        return cb(new Error('Please upload an image'));
      }
      cb(undefined, true);
    }
});
  
  // POST route for uploading avatar
router.post('/user/me/avatar', auth.userAuth, upload.single('avatar'), controller.createAvatar, (error, req, res, next) => {
  res.status(400).send({ error: error.message });
});
  
router.delete("/user/me/avatar", auth.userAuth, controller.deleteAvatar)

router.get('/user/:id/avatar', controller.getAvatar)


//item routes
// POST route to store item data
router.post('/users/item', auth.userAuth, upload.single('avatar'), controller.createItem, (error, req, res, next) => {
  res.status(400).send({error:error.message})
});
// PUT route to update an item
router.put('/users/:itemId', controller.updateItem);

// GET route to get a single item by ID
router.get('/users/:itemId', controller.getItemById);
router.get('/items/:category', controller.getAllItems);

// DELETE route to delete an item by ID
router.delete('/item/:itemId', controller.deleteItemById);

//feedback
router.post("/feedback/create", auth.userAuth, controller.createFeedback);
module.exports = router;

//get item image
router.get('/item/:id/avatar', controller.getItemImage)