const mongoose = require("mongoose")
const bcrypt = require("bcryptjs")
const jwt = require("jsonwebtoken") // creating token

require("dotenv").config({ path: './configuration/config.env'}) //giving path

const userSchema = new mongoose.Schema({
    firstName: {
        type: String,
        required: true,
    },
    lastName: {
        type:String,
    },
    email:{
        type: String,
        required: true,
        unique : true,
        trim: true,
    },
    password:{
        type:String,
        required:true,
        trim: true,
        minlength: 8,
    },
    tokens:[{
        token:{
            type: String,
            require: true
        }
    }],
    avatar:{
        type: Buffer
    }
})

userSchema.pre("save", async function(next){ // to encrypt the password before saving. 
    const user = this // 

    if(user.isModified("password")) { // if user password changed
        user.password = await bcrypt.hash(user.password, 8) 
    }
    next() // moving to the next step or inroder to let the server know that we are done
})

userSchema.methods.generateAuthToken = async function (){//call this function for  creating token from controller
    const user = this // refering to a particular user data
    const token = jwt.sign({_id:user._id.toString()}, process.env.TOKEN_SIGNATURE ) // to fetch the data from config.env file
    
    user.tokens = user.tokens.concat({token:token}) // stores the token in database
    await user.save() // save to database

    return token
}

userSchema.statics.findByCredentials = async (email, password) => { // to check if the user data is ther in database or not
    const users = await user.findOne({email:email}) // in user tbale find one row. check email in email field

    if(!users){
        throw "Invalid Credential"
    }

    const isMatch = await bcrypt.compare(password,users.password)
    if (!isMatch) {
        throw "Invalid Credential"
    }

    return users
}

const user = mongoose.model("user", userSchema) //creating user table (schema is the field type)
module.exports = user
