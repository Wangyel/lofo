const mongoose = require("mongoose")

const itemSchema = new mongoose.Schema({
  date: {
    type: Date,
    required: true
  },
  time: {
    type: String,
    required: true
  },
  location: {
    type: String,
    required: true
  },
  owner: {
    type: String,
    required: true
  },
  contact: {
    type: String,
    required: true
  },
  designation: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  category: {
    type: String,
    required: true,
    enum : ['lost', 'found']
  },
  avatar: {
    type: Buffer
  }
});

const Item = mongoose.model('Item', itemSchema);

module.exports = Item;