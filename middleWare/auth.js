const jwt = require("jsonwebtoken") // create and verify token
const userModel = require("../model/userModel")
const adminModel = require("../model/adminModel")

require("dotenv").config({path: '..configuration/config.env'})

const userAuth = async (req, res, next) => {
    try{
        const {cookies} = req  // extract only cookie value from req body
        var token = ""
        if('session_id' in cookies){
            token = cookies.session_id
        }else{
            throw ""
        }

        const decoded = jwt.verify(token, process.env.TOKEN_SIGNATURE) // verify token

        //const checking for admin witht he id and also checking if the token is present.
        const user = await userModel.findOne({_id:decoded._id,"tokens.token":token})
        if (!user){
            throw ""
        }

        req.token = token
        req.user = user
        next()
    }catch(e){
        res.status(401).send({error:"Please Authenticate."})
    }
}

const adminAuth = async (req, res, next) => {
    try{
        const {cookies} = req  // extract only cookie value from req body
        var token = ""
        if('session_id' in cookies){
            token = cookies.session_id
        }else{
            throw ""
        }

        const decoded = jwt.verify(token, process.env.TOKEN_SIGNATURE) // verify token

        //const checking for admin witht he id and also checking if the token is present.
        const admin = await adminModel.findOne({_id:decoded._id,"tokens.token":token})
        if (!admin){
            throw ""
        }

        req.token = token
        req.admin = admin
        next()
    }catch(e){
        res.status(401).send({error:"Please Authenticate."})
    }
}
module.exports = {
    userAuth,
    adminAuth
}